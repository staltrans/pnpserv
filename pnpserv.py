#!/usr/bin/python -tt
#
# Multicast telephone discovery
#
#
# Author: Filip Polsakiewicz <filip.polsakiewicz@snom.de>, boris_t <boris@talovikov.ru>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import socket
import struct
import sys
import re
import netifaces
import random
import string
import os.path
import ConfigParser
import logging

class Configure:

    __configs = (
        '/etc/pnpserv.conf',
        '/usr/local/etc/pnpserv.conf',
        'pnpserv.conf'
    )

    __myconfig = None
    __myconfig_pars = None

    def __init__(self):
        for config in self.__configs:
            if os.path.isfile(config):
                self.__myconfig = config
                break
        if not self.__myconfig:
            die("Config file not found")
        self.__myconfig_pars = ConfigParser.ConfigParser()
        self.__myconfig_pars.read(self.__myconfig)

    def getValCustom(self, section, var):
        try:
            return self.__myconfig_pars.get(section, var)
        except ConfigParser.NoOptionError:
            return None
        except ConfigParser.NoSectionError:
            return None

    def getVal(self, var):
        return self.getValCustom('global', var)


def parse_package(text):
    regexp = re.compile(
        ur"(?:^(?P<method>[A-Z]+) .+?(?P<mac>[0-9,a-f]{12})@\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3} .*)|(?P<via>^Via: .+?(?P<ip>\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}):(?P<port>\d{1,5})\S*)|(?P<from>^From: .+?\S*)|(?P<to>^To: .+?\S*)|(?:^Call-ID: (?P<call_id>.+))|(?P<cseq>^CSeq: .+)|(?:^Event: .+?vendor=\"(?P<vendor>.+?)\";model=\"(?P<model>.+?)\";version=\"(?P<version>.+?)\")", re.M | re.U )
    iterator = regexp.finditer(text)
    group_name_by_index = dict([(v, k) for k, v in regexp.groupindex.items()])
    result = {}
    for match in iterator:
        for group_index, group in enumerate(match.groups()):
            if group :
                result[group_name_by_index[group_index + 1]] = group
    return result


def get_ip_address(iface = None):
    interfaces = netifaces.interfaces()
    if not iface in interfaces:
      iface = None
    ifnum = 0
    for ifname in interfaces:
        ifnum = ifnum + 1
        if ifname == 'lo':
            continue
        if iface == None or ifname == iface:
            ifconfigs = netifaces.ifaddresses(ifname).get(netifaces.AF_INET)
            if ifconfigs != None:
                for ifconf in ifconfigs:
                    return (ifnum, ifconf['addr'])


def gen_id():
    return ''.join(random.choice(string.letters + string.digits) for _ in range(12))


cnf = Configure()

listen = cnf.getVal('listen')
if listen:
    ifnum, ip_adr = get_ip_address(listen)
else:
    ifnum, ip_adr = get_ip_address()

url = cnf.getVal('url')

log_conf = {}
log_conf['format'] = '[%(asctime)s] %(levelname)s: %(message)s'

debug = cnf.getVal('debug')
if debug == 'yes':
    log_conf['level'] = logging.DEBUG
else:
    log_conf['level'] = logging.INFO

log = cnf.getVal('log')
if log:
    log_conf['filename'] = log

logging.basicConfig(**log_conf)

logging.info("Start Multicast PnP Provisioning Server")
if not url:
    logging.error('Option url required')
    exit()
logging.debug("Default provisioning URI is %s" % url)
logging.debug("Listen IP Address is :: %s" % ip_adr)

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
sock.bind(('224.0.1.75', 5060))
mreq = struct.pack('4sl', socket.inet_aton('224.0.1.75'), ifnum)
sock.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq)

while True:
    subs = sock.recv(10240)

    logging.debug("receive\n<<\n%s\n<<" % subs)

    pkg = parse_package(subs)

    try:
        if pkg['method'] == 'SUBSCRIBE':
            # Create a socket to send data
            sendsock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
            sendsock.bind(('%s' % ip_adr, 1036))
            call_id = gen_id() + "@" + ip_adr

            # Now send a NOTIFY with the configuration URL

            custom_url = None
            for section in [pkg['mac'], pkg['model'], pkg['vendor']]:
                custom_url = cnf.getValCustom(section.lower(), 'url')
                if custom_url:
                    break

            if custom_url:
                prov_uri = custom_url
            else:
                prov_uri = url
            for token in ['vendor', 'model', 'mac', 'version']:
                prov_uri = re.sub('{' + token + '}', pkg[token].lower(), prov_uri)

            notify = "NOTIFY sip:%s:%s SIP/2.0\r\n" % (pkg['ip'], pkg['port'])
            notify += pkg['via'] + "\r\n"
            notify += "Max-Forwards: 20\r\n"
            notify += "Contact: <sip:%s:1036;transport=TCP;handler=dum>\r\n" % ip_adr
            notify += pkg['to'] + "\r\n"
            notify += pkg['from'] + "\r\n"
            notify += "Call-ID: %s\r\n" % call_id
            notify += "CSeq: 3 NOTIFY\r\n"
            notify += "Content-Type: application/url\r\n"
            notify += "Subscription-State: terminated;reason=timeout\r\n"
            notify += "Event: ua-profile;profile-type=\"device\";vendor=\"%s\";model=\"%s\";version=\"%s\"\r\n" % (pkg['vendor'], pkg['model'], pkg['version'])
            notify += "Content-Length: %i\r\n" % (len(prov_uri))
            notify += "\r\n%s" % prov_uri

            logging.info("Sending NOTIFY with URI :: %s" % prov_uri)
            logging.debug("send\n>>\n%s\n>>" % notify)

            sendsock.sendto(notify, ("%s" % pkg['ip'], int(pkg['port'])))
    except KeyError:
        pass
