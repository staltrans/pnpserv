# Multicast telephone discovery

Форк [PnP Server](https://sourceforge.net/projects/pnpserv/)

## Пример

После запуска скрипта 
```
./pnpserv.py -u http://192.168.10.1/{vendor}/{model}/  -l eth1 -v
```

в логах веб-сервера появятся записи:

Телефон [SNR VP-7050](http://shop.nag.ru/catalog/archive/12389.SNR-VP-7050)
```
192.168.10.86 - - [27/May/2016:14:17:04 +0500] "GET /snr/vp-7050/f0C00620000.cfg HTTP/1.1" 444 0 "-" "-"
192.168.10.86 - - [27/May/2016:14:17:07 +0500] "GET /snr/vp-7050/4c3b74013120.cfg HTTP/1.1" 444 0 "-" "-"
```

Телефон [SNR VP-51](http://shop.nag.ru/catalog/02602.IP-Telefony/08319.SNR/15611.SNR-VP-51)
```
192.168.10.91 - - [27/May/2016:14:17:04 +0500] "GET /nag/snr-vp-51/ HTTP/1.0" 444 0 "-" "NAG SNR-VP-51 50.143.3.11 0c:11:05:02:1b:6c"
```

Телефон LAVA Telecom [LV-2SB](http://shop.nag.ru/catalog/02602.IP-Telefony/15138.LavaTelecom/15077.LV-2SB)
```
192.168.10.62 - - [27/May/2016:17:05:31 +0500] "GET /nag/lv-2sb/factory_803.bin HTTP/1.1" 444 0 "-" "NAG LV-2SB 1.0.3.62 00:1f:c1:1a:9b:aa"
192.168.10.62 - - [27/May/2016:17:05:31 +0500] "GET /nag/lv-2sb/cfg001fc11a9baa HTTP/1.1" 444 0 "-" "NAG LV-2SB 1.0.3.62 00:1f:c1:1a:9b:aa"
192.168.10.62 - - [27/May/2016:17:05:31 +0500] "GET /nag/lv-2sb/cfg001fc11a9baa.xml HTTP/1.1" 444 0 "-" "NAG LV-2SB 1.0.3.62 00:1f:c1:1a:9b:aa"
```

Для телефонов SNR VP-7050 и SNR VP-51 требуется предварительная настройка.

## Установка

```
# cd /opt
# git clone http://bitbucket.org/staltrans/pnpserv.git
# ln -s /opt/pnpserv/systemd/pnpserv.service /etc/systemd/system/
# service pnpserv start
# ln -s /opt/pnpserv/logrotate/pnpserv.conf /etc/logrotate.d/
```

## Ссылки

 * [Phone Provisioning in Asterisk](https://wiki.asterisk.org/wiki/display/AST/Phone+Provisioning+in+Asterisk)
 * [Dynamic Phone Provisioning with res phoneprov and TFTP](http://etel.wiki.oreilly.com/wiki/index.php/Dynamic_Phone_Provisioning_with_res_phoneprov_and_TFTP)
 * [Автопровизия (Автонастройка) телефонов Polycom с помощью Asterisk](https://habrahabr.ru/post/218227/)
 * [VoIP зоопарк — Provisioning](https://habrahabr.ru/post/237951/)
 * [Система автоконфигурации для VoIP устройств на коленке](https://habrahabr.ru/post/134851/)
 * [Linksys SPA Provisioning](http://wiki.opennet.ru/Linksys_SPA_Provisioning)
 * [SIPPing](https://github.com/pbertera/SIPPing)
 * [GXP21xx/GXP14xx Auto-configuration](http://grandstream.com/products/gxp_series/general/documents/gxp14xx_21xx_plug_and_play.pdf)
 * [Глава 7. Протокол инициирования сеансов связи – SIP](http://niits.ru/public/2003/011.pdf)
 * [GS3/opt/gemeinschaft/sbin/gs-sip-ua-config-responder/gs-sip-ua-config-responder](https://github.com/amooma/GS3/blob/master/opt/gemeinschaft/sbin/gs-sip-ua-config-responder/gs-sip-ua-config-responder)
 * [[asterisk-users] snom mass deploy help](http://lists.digium.com/pipermail/asterisk-users/2009-June/233531.html)
